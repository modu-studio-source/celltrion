$(window).on("resize", function () {
    var list = $('#partnersList');
    var map = $('#partnersMap');
    //var body = $('#partnersCountry');
    var multiView = ($(window).width() <= 668) ? 3 : 7 ;
    var index = 0;
    var hash = window.location.hash;
    var item = $(list).find("li");

    if (hash) {
        index = /\d+/.exec(hash)[0];
        index = (parseInt(index) || 1) ;
        sele($(item).eq(index-1));
        sele($(map).children().eq(index-1));
        //sele($(body).children().eq(index-1));
    }



    $(list).touchSlider({
        roll : false,
        view : multiView,
        resize : true,
        page : function(e){
            return Math.ceil(index/multiView);
        }(),
        initComplete : function (e) {
            var _this = this;

            $(item).find('a').each(function (i, el) {
                $(this).on('click',function(){
                    sele($(this).parent('li'));
                    sele($(map).children().eq(i));
                    //sele($(body).children().eq(i));
                    window.location.hash = i+1;
                    return false;
                });
            });
        },
        btn_prev : $("#partnersBtnPrev"),
        btn_next : $("#partnersBtnNext")
    });
    function sele(el) {
        $(el).addClass('on').find('img').attr('src',function(){return this.src.replace("_off.","_on.");}).end()
            .siblings().removeClass('on').find('img').attr('src',function(){return this.src.replace("_on.","_off.");}
        );
    }
}).resize();
